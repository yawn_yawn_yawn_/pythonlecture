# python基礎
## やったこと
1. 環境構築
    - python
    - pycharm
    - (pip)
2. 変数・定数
3. 配列
4. 組
5. 辞書
6. 型判定
7. if文
8. for文・while文
9. 条件式
10. ファイル
11. 関数
12. クラス・継承
13. 予約語
14. 例外処理


## やること？
- python, pip, pycharmのインストール
- モジュール管理について少し．pipとは？とかほんの少し
- (print input)
- if, for, while
- リスト，タプル，ディクト
- (try - except)
- (time.sleep)
- .format()とか
- numpy
- pandas
- ファイル操作
- lambda式
- 関数とクラス

ミニ入門なので．．．そんなにいらないか？
