# for文
# range
print("range(5)")
for i in range(5): # 0 ~ 4
    print("{}".format(i))

print("-" * 10)

print("range(1, 4)")
for i in range(1, 4): # 1 ~ 3
    print("{}".format(i))

# -------------------------------------
# list
list = ['ねこ', 'いぬ', 'とり', 'さかな']
for val in list:
    print("{}".format(val))
    

dict = {'January':'1月', 
        'February':'2月', 
        'March':'3月',}
for key, value in dict.items():
    print("key:{k}, value:{v}".format(k=key, v=value))

# -------------------------------------
# while文
num = 0
while num < 5:
    print("{}".format(num))
    num += 1
    
