"""モジュールドキュメント"""
class Person:
    """Personクラスドキュメント"""
    def __init__(self, *args):
        """関数レベルドキュメント"""
        self.name = args[0]
        self.age = args[1]

    def __str__(self):
        return "{}です".format(self.name)

    def talk_age(self):
        print("{}歳です".format(self.age))


class Student(Person):
    def __init__(self, *args):
        super().__init__(*args)
        self.id = args[2]

    def __str__(self):
        return "学籍番号は{}です".format(self.id)

# オブジェクトの作成
me = Student('大橋', 20, 't317013')
print(me)
me.talk_age()
