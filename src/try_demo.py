a = 10
b = 2

try:
    # 例外が怒るかもしれない処理
    c = a / b
except ZeroDivisionError:
    # 例外が起こった時の処理
    print('0で徐算が行われた')
else:
    # 例外が起こらなかった時の処理
    print('計算終了 {}'.format(c))
finally:
    # 絶対行われる処理
    print('try文終了')
