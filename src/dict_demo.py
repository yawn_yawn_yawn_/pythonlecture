dict = {'apple': 300, 'orange': 200, 'peach': 500}

dict = {'apple': 300,
        'orange': 200,
        'peach': 500}
    

# 値の取得
dict = {'apple': 300, 'orange': 200, 'peach': 500}
print(dict['apple'])
print(dict.keys())
print(dict.values())
print(dict.items())


# 値の変更，追加
dict = {'apple': 300, 'orange': 200}
dict['apple'] = 400
print(dict) # {'apple': 400, 'orange': 200}

dict.update({'orange': 300})
print(dict) # {'apple': 400, 'orange': 300}

dict.update({'banana': 100}) 
print(dict) # {'apple': 400, 'orange': 300, 'banana': 100}


# 要素の削除
dict = {'apple': 300, 'orange': 200, 'peach': 500}
del dict['apple']
print(dict) # {'orange': 200, 'peach': 500}

dict = {'apple': 300, 'orange': 200, 'peach': 500}
dict.pop('apple')
print(dict) # {'orange': 200, 'peach': 500}

dict = {'apple': 300, 'orange': 200, 'peach': 500}
dict.clear()
print(dict) # {}
