tup = ()
tup = (1, 2, 3)

# 書き換えはできない
tup[0] = 3    # error

# 結合
tup = (1, 2, 3,)
tup += (4, 5)
print(tup)  # (1, 2, 3, 4, 5)

# アクセスなど
tup = (1, 2, 3,)
print(tup.index(3)) # 要素を指定してインデックスを調べる
print(tup.count(2)) # 要素の数を調べる

# 注意: 要素が一つだけの場合は末尾に「,」をつける
# OK
tup = (1, 2, 3)
print(type(tup))

tup = (1,)
print(type(tup))

# NG
tup = (1)
print(type(tup)) # int
