# 英語(大小)，数字，アンダースコア
a = 100
b1 = 10.0
str_sample = 'string'
StrSample = "String"
_under1 = 200
__under2 = 300

print(a)
print(b1)
print(str_sample)
print(StrSample)
print(_under1)
print(__under2)

# 先頭に数字の変数名はNG
1a = 400    # error
1_a = 500   # error

# 定数
PI = 3.14
MAX_VALUE = 1024
DOCUMENT = "ドキュメント"
