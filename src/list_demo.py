# 宣言
list = []
list = [1, 2, 3]

# 要素の追加
print("要素の追加")
list = [1, 2, 3]
list.append(4) # 追加したい要素
print(list)

list = [1, 2, 3]
list.insert(1, 'D') # 追加する場所と要素
print(list)


# 要素の削除
print("要素の削除")
list = [1, 2, 3]
del list[1] # 削除したい要素のインデックス
print(list)

list = [1, 2, 3]
list.pop(1) # 削除したい要素のインデックス
print(list)

list = [1, 2, 3, 3, 4]
list.remove(3) # 削除したい要素
print(list)


# 要素を逆順に
print("要素を逆順に")
list = [1, 2, 3]
list.reverse()
print(list)


# 要素の数
print("要素の数")
list = [1, 2, 3, 3, 4]
print(list.count(3)) # カウントしたい要素


# 要素のリセット
print("要素のクリア")
list = [1, 2, 3]
list.clear()
print(list)

print()
# ------------------------
# アクセスについて
list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# 配列の長さ
print(len(list)) # 10

# index指定
print(list[0]) # 1

# 先頭から2つ
print(list[:2]) # [1, 2]

# 2番目以降のデータ
print(list[2:]) # [3, 4, 5, 6, 7, 8, 9, 10]

# 4番目，5番目のデータ
print(list[3:5]) # [4, 5]

# 後ろから2番目のデータ
print(list[-2]) # 9

# 2個おき
print(list[::2]) # [1, 3, 5, 7, 9]

# 1番目以降，7番目までのデータを，2個おきに
print(list[1:7:2]) # [2, 4, 6]
