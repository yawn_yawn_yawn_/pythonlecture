# 基本
def ave(a, b):
    c = (a + b) / 2
    return c
    
result = ave(5, 7)
print(result)


# 可変引数
# tuple
def sum(*args):
    sum = 0
    for i in args:
        sum += i
    return sum
    
print(sum(1, 2, 3, 4, 5))

# dict
def func_key(**kwargs):
    for k, v in kwargs.items():
        print("key:{}, value:{}".format(k, v))

func_key(key1='value1', key2='value2')

# lambda式
add = lambda a, b: print(a + b)

add(5, 6)
