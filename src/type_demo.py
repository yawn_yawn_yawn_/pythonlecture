int_var = 10
print(type(int_var)) # <class 'int'>

float_var = 10.5
print(type(float_var)) # <class 'float'>

string_var = 'string'
print(type(string_var)) # <class 'str'>

list_var = [1, 2, 3]
print(type(list_var)) # <class 'list'>

tuple_var = (1, 2, 3)
print(type(tuple_var)) # <class 'tuple'>

dict_var = {1:'one', 2:'two', 3:'three'}
print(type(dict_var)) # <class 'dict'>

bool_var = True
print(type(bool_var)) # <class 'bool'>
