# ファイルオープン
fp = open('read_sample.txt', mode='r', encoding='utf-8')
print(fp.read())
#print(fp.readline())
#print(fp.readlines())
fp.close()


# ----------------------
# with
with open('read_sample.txt') as f:
    print(f.read())


# ----------------------
# 書き込み
string = 'sample string'
with open('write_sample.txt', mode='w') as f:
    f.write(string)
