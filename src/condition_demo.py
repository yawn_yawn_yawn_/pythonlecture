int_var = 10
float_var = 10.0
list = [1, 2, 3, 4, 5]

print(1 in list) # True
print(2 not in list) # False

print(int_var == float_var) # True
print(int_var is float_var) # False

print(int_var != float_var) # False
print(int_var is not float_var) # True

a = True
b = False
print(a or b)   # True
print(a and b)  # False
